from turtle import *

def deplacement(x,y):
    penup()
    goto(x,y)
    pendown()

def rectangle(longeur,largeur,color):
    """cette fonction sert a créer un rectangle de couleur"""
    
    fillcolor(color)
    begin_fill()
    for i in range(2):
        forward(longeur)
        right(90)
        forward(largeur)
        right(90)
    end_fill()
    
def encadrement(longeur,largeur, epaisseur):
    """cette foncion créer un cadre"""
    speed(0)
    deplacement(-longeur / 2, largeur / 2)
    rectangle(longeur, largeur,'gold')
    deplacement((-longeur / 2) + epaisseur, (largeur / 2) - epaisseur)
    rectangle(longeur - 2 * epaisseur, largeur - 2* epaisseur,'lightsteelblue')
        
    
def cadre1():
    
    encadrement(1000, 600, 25)
    deplacement(-465,-275)

    for k in range(20):
        fillcolor("orchid")
        begin_fill()
        circle(10)
        end_fill()
        penup()
        forward((950 - 2 * 10) / 19)
        left(90)
        forward((550 - 2 * 10) / 19)
        right(90)
        pendown()

cadre1()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

        
