from turtle import *
from random import *

setup(1000, 600)


def deplacement(x, y):
    """cette fonction permet de deplacer le curseur a un point x y"""
    penup()
    goto(x, y)
    pendown()


def rectangle(longeur, largeur, color):
    """cette fonction permet de créer un rectangle de couleur"""

    fillcolor(color)
    begin_fill()
    for i in range(2):
        forward(longeur)
        right(90)
        forward(largeur)
        right(90)
    end_fill()


def encadrement(longeur, largeur, epaisseur):
    """cette fonction crée un cadre"""
    speed(0)
    deplacement(-longeur / 2, largeur / 2)
    rectangle(longeur, largeur, "gold")
    deplacement((-longeur / 2) + epaisseur, (largeur / 2) - epaisseur)
    rectangle(longeur - 2 * epaisseur, largeur - 2 * epaisseur, "lightsteelblue")


def cadre1():
    """cette fonction cree un cadre avec des cercles alignés qui
    partent d'en bas a gauche et qui arrivent en haut a droite"""

    encadrement(1000, 600, 25)
    deplacement(-465, -275)

    for k in range(20):
        fillcolor("orchid")
        begin_fill()
        circle(10)
        end_fill()
        penup()
        forward((950 - 2 * 10) / 19)
        left(90)
        forward((550 - 2 * 10) / 19)
        right(90)
        pendown()


# cadre1()


def limite():
    """cette fonction limite l'espace de creation de figure afin de ne pas depasser
    du cadre"""
    x = randint(-425, 422)
    y = randint(-225, 222)
    deplacement(x, y)


def cadre2(n):
    """cette fonction permet dessiner dans le cadre des cercles aléatoirement"""

    encadrement(1000, 600, 25)

    for k in range(n):
        speed(0)
        limite()
        fillcolor("orchid")
        begin_fill()
        circle(10)
        end_fill()


# cadre2(50)


def taille():
    """cette fonction defini aleatoirement la taille des figures"""
    t = randint(10, 30)
    return t


def cadre3(n):
    """cette fonction crée des cercles à des positions alléatoires avec les couleurs aléatoires"""

    encadrement(1000, 600, 25)

    for k in range(n):
        speed(0)
        limite()
        color = choice(
            ["moccasin", "cornflowerblue", "plum", "darkseagreen", "lightsalmon"]
        )
        fillcolor(color)
        begin_fill()
        circle(taille())
        end_fill()


# cadre3(50)


def star(n):
    """cette fonction crée une étoile"""
    for i in range(5):
        forward(n)
        right(120)
        forward(n)
        right(72 - 120)


def cadre4(n):
    """cette fontion cree des étoiles a des positons aléatoires avec des couleurs aleatoires"""

    encadrement(1000, 600, 25)

    for k in range(n):
        speed(0)
        limite()
        color = choice(
            ["moccasin", "cornflowerblue", "plum", "darkseagreen", "lightsalmon"]
        )
        fillcolor(color)
        begin_fill()
        star(taille())
        end_fill()


# cadre4(40)


def poly(a):
    """cette fonction cree un polygône avec un nombre de cotés aléatoire"""
    b = randint(3, 8)
    for i in range(b):
        forward(a)
        left(360 / b)


def cadre5(n):
    """cette fontion permet de creer des polygones avec un nombre aléatoire de cotés
    à des positions aléatoires"""

    encadrement(1000, 600, 25)

    for k in range(n):
        speed(0)
        limite()
        color = choice(
            ["moccasin", "cornflowerblue", "plum", "darkseagreen", "lightsalmon"]
        )
        fillcolor(color)
        begin_fill()
        poly(taille())
        end_fill()


# cadre5(50)


def cadre6(n):
    """cette fontion permet de creer alétoirement des cercles,des étoiles ou des polygones"""

    encadrement(1000, 600, 25)

    for p in range(n):
        speed(0)
        limite()
        figure = choice([circle, star, poly])
        color = choice(
            ["moccasin", "cornflowerblue", "plum", "darkseagreen", "lightsalmon"]
        )
        fillcolor(color)
        begin_fill()
        figure(taille())
        end_fill()


# cadre6(50)


def epaisseur_trait():
    """cette fonction determine l'épaisseur du trait en fonction de la taille du flocon"""
    e = randint(7, 20)
    if e < 12:
        width(2)
    else:
        width(4)

    return e


def aller_retour(longueur):
    """cette fonction trace un trait et revient à la position initiale"""

    forward(longueur)
    backward(longueur)


def trace_branche(longeur_tige, longeur_embranchement):
    """cette fonction trace une branche avec deux embranchements"""

    forward(longeur_tige)
    left(45)
    aller_retour(longeur_embranchement)
    right(90)
    aller_retour(longeur_embranchement)
    left(45)


def flake(n, branches):
    """cette fonction permet de creer un flocon"""

    for i in range(branches):
        left(360 / branches)

        trace_branche(n, n / 2)
        trace_branche((2 / 3) * n, (1 / 3) * n)

        backward(n + (2 / 3) * n)


def cadre7(n):
    """cette fonction permet de creer aléatoirement des flocons dans le cadre"""

    encadrement(1000, 600, 25)

    for k in range(n):
        speed(0)
        limite()
        color = choice(["lightblue", "cornflowerblue", "aliceblue", "dodgerblue"])
        pencolor(color)
        flake(epaisseur_trait(), randint(5, 9))


cadre7(50)
